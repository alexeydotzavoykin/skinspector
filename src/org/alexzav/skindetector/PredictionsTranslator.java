package org.alexzav.skindetector;

import java.util.HashMap;
import java.util.Locale;

public class PredictionsTranslator {
    private HashMap<String, String> ruDiseas = new HashMap<>();
    private HashMap<String, String> enDiseas = new HashMap<>();
    private HashMap<Locale, HashMap<String, String>> localizationSets = new HashMap<>();
    private static PredictionsTranslator instance;

    private PredictionsTranslator() {
        updateRussianLocale();
        updateEnglishLocale();
    }

    void updateRussianLocale() {
        ruDiseas.put("acne", "Угри");
        ruDiseas.put("basal cell carcinoma", "Базалиома");
        ruDiseas.put("boils", "Чирей");
        ruDiseas.put("herpes", "Герпес");
        ruDiseas.put("hives", "Крапивница");
        ruDiseas.put("melanoma", "Меланома");
        ruDiseas.put("scabies", "Чесотка");
        localizationSets.put(new Locale("ru_ru"), ruDiseas);
    }

    void updateEnglishLocale() {
        enDiseas.put("acne", "Acne");
        enDiseas.put("basal cell carcinoma", "Basal cell carcinoma");
        enDiseas.put("boils", "Boils");
        enDiseas.put("herpes", "Herpes");
        enDiseas.put("hives", "Hives");
        enDiseas.put("melanoma", "Melanoma");
        enDiseas.put("scabies", "Scabies");
        localizationSets.put(new Locale("en_EN"), enDiseas);
    }

    public static String Translate(String locale, String line) {
        if (instance == null) {
            instance = new PredictionsTranslator();
        }
        try {
            if (instance.localizationSets.get(new Locale(locale.toLowerCase())) != null) {
                if (instance.localizationSets.get(new Locale(locale.toLowerCase())).get(line.trim()) == null) {
                    System.out.println("NOT TRANSLATABLE " + line + " TO LANGUAGE " + locale);
                }
                return instance.localizationSets.get(new Locale(locale.toLowerCase())).get(line.trim());
            }
            else {
                if (instance.localizationSets.get(new Locale("en_EN")).get(line.trim()).isEmpty()) {
                    System.out.println("RUNTIME FAILURE: DEFAULT VOCABULARY NOT INITIALIZED!");
                }
                return instance.localizationSets.get(new Locale("en_EN")).get(line.trim());
            }
        } catch (Exception exception) {
            return "No value found";
        }
    }
}
