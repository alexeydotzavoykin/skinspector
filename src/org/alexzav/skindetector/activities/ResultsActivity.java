package org.alexzav.skindetector.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.alexzav.skindetector.PredictionsTranslator;
import org.alexzav.skindetector.R;

import java.util.*;

public class ResultsActivity extends Activity {
    final static String GOOGLE_SEARCH_QUERY = "https://www.google.com/search?q=";
    final static String PRINTABLE_RESULTS_STRING = "resultsPrintable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        StringBuilder resultsLine = new StringBuilder();
        Locale locale = getResources().getConfiguration().locale;
        String pairsLine = getIntent().getStringExtra(PRINTABLE_RESULTS_STRING);
        if (pairsLine.length() > 2) {
            TextView textView = (TextView) findViewById(R.id.results_text_view_id);
            textView.setText(R.string.results_string);
            pairsLine = pairsLine.substring(1, pairsLine.length()-1);
            String[] pairs = pairsLine.split(",");
            for (String pair : pairs) {
                String[] keyValue = pair.split("=");
                try {
                    resultsLine
                            .append(PredictionsTranslator.Translate(locale.toString(), keyValue[0]))
                            .append("!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        final ArrayList<String> resultLink = new ArrayList<>(Arrays.asList(resultsLine.toString().split("!")));
        Button mFRButton = findViewById(R.id.idButtonFr);
        Button mSRButton = findViewById(R.id.idButtonSr);
        Button mTRButton = findViewById(R.id.idButtonTr);
        mFRButton.setVisibility(View.GONE);
        mSRButton.setVisibility(View.GONE);
        mTRButton.setVisibility(View.GONE);
        if (resultLink.size() > 0 && !resultLink.get(0).equals("")) {
            mFRButton.setVisibility(View.VISIBLE);
            mSRButton.setVisibility(View.GONE);
            mTRButton.setVisibility(View.GONE);
            mFRButton.setText(resultLink.get(0));
            mFRButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(GOOGLE_SEARCH_QUERY + resultLink.get(0)));
                    startActivity(browserIntent);
                }
            });
            if (resultLink.size() > 1) {
                mSRButton.setVisibility(View.VISIBLE);
                mFRButton.setVisibility(View.VISIBLE);
                mTRButton.setVisibility(View.GONE);
                mSRButton.setText(resultLink.get(1));
                mSRButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(GOOGLE_SEARCH_QUERY + resultLink.get(1)));
                        startActivity(browserIntent);
                    }
                });
                if (resultLink.size() > 2) {
                    mSRButton.setVisibility(View.VISIBLE);
                    mFRButton.setVisibility(View.VISIBLE);
                    mTRButton.setVisibility(View.VISIBLE);
                    mTRButton.setText(resultLink.get(2));
                    mTRButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent browserIntent = new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(GOOGLE_SEARCH_QUERY + resultLink.get(2)));
                            startActivity(browserIntent);
                        }
                    });
                }
            }
        }
        }

    public void onContinueDetection(View view) {
        Intent intent = new Intent(ResultsActivity.this, ClassifierActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void onStartAgain(View view) {
        Intent intent = new Intent(ResultsActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
