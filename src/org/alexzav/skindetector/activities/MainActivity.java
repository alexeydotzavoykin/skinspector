package org.alexzav.skindetector.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import org.alexzav.skindetector.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savdInstanceState) {
        super.onCreate(savdInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, ClassifierActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void onSettings(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void onSinglePhoto(View view) {
        Intent intent = new Intent(MainActivity.this, SingleShotActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_from_left);
    }
}
