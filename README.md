# TensorFlow Android Camera Skin Diseases Detection App

This project was build on the Tensorflow Demo (https://github.com/googlecodelabs/tensorflow-for-poets-2/tree/master/android/tfmobile) with custom Tensorflow Model in use.

It was created for educational purposes only, and will not implement any earnings further.

Performance of application is extremely low, since I failed to find really huge image package of skin diseases. Still, this example contain pretty clear code and could be easily improved further. 

![Main menu](images/1.jpg)
![Main view](images/2.jpg)
![Results](images/3.jpg)
![About](images/4.jpg)

Trello for project: https://trello.com/b/Q16pzmNL/diploma

Contact me on alexmu09@mail.ru
