package org.alexzav.skindetector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

import org.alexzav.skindetector.R;

public class SettingsActivity extends Activity {
    private final String HELPT_TEXT_IDENTIFICATION_STRING = "helpTextIdentification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }


    public void onAbout(View view) {
        String ABOUT_IDENTIFICATION_STRING = "about";
        onButtonClick(ABOUT_IDENTIFICATION_STRING + "_text");
    }

    public void onPrivacy(View view) {
        String PRIVACY_POLICY_IDENTIFICATION_STRING = "privacy";
        onButtonClick(PRIVACY_POLICY_IDENTIFICATION_STRING + "_text");
    }

    public void onDiseases(View view) {
        String DISEASES_IDENTIFICATION_STRING = "diseases";
        onButtonClick(DISEASES_IDENTIFICATION_STRING + "_text");
    }

    private void onButtonClick(String buttonString) {
        Intent intent = new Intent(SettingsActivity.this, AboutActivity.class);
        intent.putExtra(HELPT_TEXT_IDENTIFICATION_STRING, buttonString);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void onMain(View view) {
        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
