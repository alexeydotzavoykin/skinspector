package org.alexzav.skindetector.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import org.alexzav.skindetector.R;

public class AboutActivity extends Activity {
    private final String HELP_TEXT_IDENTIFICATION_STRING = "helpTextIdentification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        TextView resultsTextView = (TextView)findViewById(R.id.text_view_id);
        String textKey = getIntent().getStringExtra(HELP_TEXT_IDENTIFICATION_STRING);
        Resources res = getApplicationContext().getResources();
        int stringId = res
                .getIdentifier(textKey, "string", getApplicationContext().getPackageName());
        String message = getResources().getString(stringId);
        resultsTextView.setText(message);
    }

    public void onAboutClose(View view) {
        Intent intent = new Intent(AboutActivity.this, SettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
